﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
    public GameObject player;

    private Vector3 offset;

    void Start()
    {
        this.offset = player.transform.position;
        this.offset.z = this.transform.position.z;
        this.transform.position = this.offset;
        this.offset = this.transform.position - player.transform.position;
    }

    void LateUpdate()
    {
        this.transform.position = player.transform.position + this.offset;
    }
}
