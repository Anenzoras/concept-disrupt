﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class PlayerDeath : MonoBehaviour
{
	public LayerMask trapLayer;
	public AudioClip hit;

	void OnTriggerEnter2D(Collider2D other)
    {
        if (((1 << other.gameObject.layer) & trapLayer.value) != 0)
        {
			this.Die();
        }
    }

	private void Die()
	{
		// TODO play death sound
		SoundManager.instance.PlaySingle(hit);
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}
}
