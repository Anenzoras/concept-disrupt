﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class PlayerSuccess : MonoBehaviour
{
	public LayerMask exitLayer;
	public AudioClip win;
	public GameObject WinCanvas;

	void OnTriggerEnter2D(Collider2D other)
    {
        if (((1 << other.gameObject.layer) & exitLayer.value) != 0)
        {
			this.NextLevel();
        }
    }

	private void NextLevel()
	{
		// play win sound
		SoundManager.instance.PlaySingle(win);

		string nextLevel = "";

		switch (SceneManager.GetActiveScene().name)
		{
			case "Level1":
				nextLevel = "Level2";
				break;
			case "Level2":
				nextLevel = "Level3";
				break;
			case "Level3":
				nextLevel = "Level4";
				break;
			case "Level4":
				nextLevel = "Level5";
				break;
			default:
				// do nothing
				break;
		}

		if (nextLevel == "")
		{
			// last level
			this.Win();
			return;
		}

		SceneManager.LoadScene(nextLevel);
	}

	private void Win()
	{
		// print the text on screen
		WinCanvas.SetActive(true);
	}
}
