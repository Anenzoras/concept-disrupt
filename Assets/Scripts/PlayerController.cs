﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    public BoxCollider2D left;
    public BoxCollider2D right;
    public BoxCollider2D bottom;
    public LayerMask BLOCK_MASK;
    public LayerMask LINE_MASK;

    public float speed = 5.0f;
    public float acceleration = 5.0f;
    public float maxSpeed = 10.0f;
    public float jumpMaximum = 50.0f;
    public float jumpModifier = 0.5f;
    public float dashPower = 30.0f;
    public float divePower = 25.0f;
    public float dashCooldown = 1.0f; // in seconds
    public float noCollisionCooldown = 0.7f; // in seconds

    // sounds
    public AudioClip jumpSound;
    public AudioClip dashSound;

    private Rigidbody2D rigidbody2D;
    private LayerMask jumpMask;
    public float jumpPower = 0.0f;
    private float timeDash = 0.0f;
    private float timeNoCollision = 0.0f;
    private bool isLookingRight = true;

    private const int PLAYER_MASK_VALUE = 8;
    private const int LINE_MASK_VALUE = 10;

    // Use this for initialization
    void Start ()
    {
        jumpMask = BLOCK_MASK | LINE_MASK;

        rigidbody2D = this.GetComponent<Rigidbody2D> ();
        rigidbody2D.freezeRotation = true;
    }

    // deal with dash
    // code clarification
    float dash()
    {
        // start cooldown
        timeNoCollision = Time.time + noCollisionCooldown;
        timeDash = Time.time + dashCooldown;

        // remove collision
        Physics2D.IgnoreLayerCollision(PLAYER_MASK_VALUE, LINE_MASK_VALUE);

        // remove movement
        rigidbody2D.velocity = new Vector2(0.0f, 0.0f);

        // play sound
        SoundManager.instance.PlaySingle(dashSound);

        // dash
        return isLookingRight ? dashPower : - dashPower;
    }

    // deals with jumps
    // code clarification
    void jump(ref Vector2 force)
    {
        bool hasJumped = true;

        jumpPower = (jumpMaximum * jumpModifier);

        if (bottom.IsTouchingLayers(jumpMask))
        {
            // GROUNDED
        }
        else if (left.IsTouchingLayers(jumpMask))
        {
            // WALL_AT_LEFT: WALL JUMP RIGHT
            force.x = jumpMaximum * 0.9f;
        }
        else if (right.IsTouchingLayers(jumpMask))
        {
            // WALL_AT_RIGHT: WALL JUMP LEFT
            force.x = -jumpMaximum * 0.9f;
        }
        else
        {
            jumpPower = 0.0f;
            hasJumped = false;
        }

        force.y = jumpPower;

        // "friction" + sound
        if (hasJumped)
        {
            rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, 0.0f);

            SoundManager.instance.PlaySingle(jumpSound);
        }
    }

    // when continuing pressing "jump" after jump, to go a bit higher
    // code clarification
    float smoothJump()
    {
        jumpPower = jumpPower - (jumpPower * jumpModifier);

        return jumpPower;
    }

    // deals with simple moving by pressing the sticks
    // code clarification
    float move()
    {
        // simple move
        float res = Input.GetAxis("Horizontal");
        if (res > 0.0f)
        {
            isLookingRight = true;
        }
        else if (res < 0.0f)
        {
            isLookingRight = false;
        }

        bool dirChanged = ((res >= 0.0F) && (rigidbody2D.velocity.x < 0.0f))
            || ((res <= 0.0F) && (rigidbody2D.velocity.x > 0.0f));

        if(bottom.IsTouchingLayers(jumpMask))
        {
            if(dirChanged)
            {
                rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x * 0.5f,
                    rigidbody2D.velocity.y);
            }

            res *= acceleration;
        }
        else
        {
            // aerial has less impact
            if(dirChanged)
            {
                rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x * 0.9f,
                    rigidbody2D.velocity.y);
            }

            res *= acceleration * 0.5f;
        }

        return res;
    }

    // Update is called once per frame
    void FixedUpdate ()
    {
        Vector2 force = Vector2.zero;

        if (timeNoCollision > Time.time)
        {
            // we are currently dashing, keep doing it without falling
            rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, 0.0f);

            return;
        }

        // reset to normal state if not dashing
        Physics2D.IgnoreLayerCollision(PLAYER_MASK_VALUE, LINE_MASK_VALUE, false);

        // deal with movement here
        if (Input.GetKeyDown(KeyCode.LeftShift) && (timeDash <= Time.time))
        {
            force.x = dash();
        }
        else
        {
            if (Input.GetButtonDown("Jump"))
            {
                jump(ref force);
            }
            else if (Input.GetButton("Jump"))
            {
                force.y = smoothJump();
            }

            force.x += move();
        }

        force.x = force.x * speed;
        force.y = force.y * speed;

        rigidbody2D.AddForce (force);

        // limit max speed (not if dashing)
        force.x = rigidbody2D.velocity.x;
        force.y = rigidbody2D.velocity.y;
        if((timeNoCollision <= Time.time) && (Mathf.Abs(force.x) >= maxSpeed))
        {
            force.x = maxSpeed * (Mathf.Abs(force.x) / force.x);
        }
        if((timeNoCollision <= Time.time) && (Mathf.Abs(force.y) >= maxSpeed))
        {
            force.y = maxSpeed * (Mathf.Abs(force.y) / force.y);
        }
        rigidbody2D.velocity = force;
    }
}

/*

pour le court-circuit de mouvement, utiliser le booléen de changement de
direction et la vitesse, et freiner fort si dans l'autre sens ou si stop
(probablement un truc comme : if detection then rigidbody2D.velocity.x /= 2.0f)
*/
